package com.example.vietpham.researchdboffline.activity.room

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.vietpham.researchdboffline.BookRoomDatabase
import com.example.vietpham.researchdboffline.R
import kotlinx.android.synthetic.main.activity_room_detail_book.*

class RoomDetailBookActivity : AppCompatActivity() {
    var userId : Long = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_room_detail_book)
        initData()
        btUpdate.setOnClickListener {
            val id = intent.getLongExtra("bookId", 0)
            val intent = Intent(this@RoomDetailBookActivity, RoomUpdateActivity::class.java)
            intent.putExtra("bookId",id)
            intent.putExtra("userId",userId)
            startActivity(intent)

        }

    }

    private fun initData() {
        val id = intent.getLongExtra("bookId", 0)
        if (id.toInt() != 0) {
            val book = BookRoomDatabase.getInstance(this).bookDao().getBookById(id)
            val user = BookRoomDatabase.getInstance(this).userDao().getUserById(book.userId)
            userId = user.id
            tvTitle.text = "Title: ${book.title}"
            tvAuthor.text = "Author: ${user.firtName} ${user.lastName}"
            tvAddress.text = "Address: " + user.address?.street + " " + user.address?.district + " " + user.address?.city
            tvPostCode.text = "Postcode: ${user.address?.postCode.toString()}"
            tvDate.text = "Date: ${book.date}"
        }
    }
}
