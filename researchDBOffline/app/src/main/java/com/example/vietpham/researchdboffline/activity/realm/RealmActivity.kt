package com.example.vietpham.researchdboffline.activity.realm

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import com.example.vietpham.researchdboffline.R
import com.example.vietpham.researchdboffline.adapter.realm.RealmBookRecyclerViewAdapter
import com.example.vietpham.researchdboffline.model.realm.Book
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.activity_main2.*

class RealmActivity : AppCompatActivity() {

    private val realm = Realm.getDefaultInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        val adapter = RealmBookRecyclerViewAdapter(realm.where<Book>().findAll())
        rvDate.layoutManager = LinearLayoutManager(this)
        rvDate.itemAnimator = DefaultItemAnimator()
        rvDate.adapter = adapter

        btDelete.setOnClickListener {
            val intent = Intent(this, RealmDeleteActivity::class.java)
            startActivity(intent)
        }

        btUpdate.setOnClickListener {
            val intent = Intent(this, RealmUpdateActivity::class.java)
            startActivity(intent)
        }

        btInsert.setOnClickListener {
            val intent = Intent(this, RealmInsertActivity::class.java)
            startActivity(intent)
        }
    }
}
