package com.example.vietpham.researchdboffline.model.room

import android.arch.persistence.room.*

@Dao
interface BookDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertBook(book: Book): Long

    @Query("SELECT * FROM book WHERE user_id = :userId")
    fun getBookByUserId(userId: Long): List<Book>

    @Query("SELECT * FROM book WHERE bookId = :Id")
    fun getBookById(Id: Long): Book

    @Query("SELECT * FROM book")
    fun getBooks(): List<Book>

    @Update
    fun updateBook(book: Book) : Int

    @Delete
    fun deleteBook(book: Book) : Int
}