package com.example.vietpham.researchdboffline.activity.realm

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import com.example.vietpham.researchdboffline.R
import com.example.vietpham.researchdboffline.adapter.realm.RealmBookRecyclerViewAdapter
import com.example.vietpham.researchdboffline.model.realm.Book
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.activity_realm_insert.*

class RealmInsertActivity : AppCompatActivity() {
    private val realm = Realm.getDefaultInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_realm_insert)
        val dogs = realm.where<Book>().findAll()
        val adapter = RealmBookRecyclerViewAdapter(realm.where<Book>().findAll())
        rvDate.layoutManager = LinearLayoutManager(this)
        rvDate.itemAnimator = DefaultItemAnimator()
        rvDate.adapter = adapter

        btInsert.setOnClickListener {
            realm.executeTransaction {
                val book1 = Book()
                book1.title = "Insert"
                book1.description = "AllHere no 1"
                book1.author = "Pham Le Huy Hoang"
                book1.imageURL = "59/59/511"
                realm.insert(book1)
            }
        }
    }
}
