package com.example.vietpham.researchdboffline.activity.room

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.example.vietpham.researchdboffline.BookRoomDatabase
import com.example.vietpham.researchdboffline.R
import com.example.vietpham.researchdboffline.model.room.Book
import com.example.vietpham.researchdboffline.model.room.User
import kotlinx.android.synthetic.main.activity_room_update.*

class RoomUpdateActivity : AppCompatActivity() {
    lateinit var book: Book
    lateinit var user: User
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_room_update)
        val id = intent.getLongExtra("bookId", 0)
        val userId = intent.getLongExtra("userId", 0)
        book = BookRoomDatabase.getInstance(this).bookDao().getBookById(id)
        user = BookRoomDatabase.getInstance(this).userDao().getUserById(userId)
        initData()
        btUpdate.setOnClickListener {
            book.title = etTitle.text.toString()
            book.date = etDate.text.toString()
            user.firtName = etfirtName.text.toString()
            user.lastName = etSecondName.text.toString()
            val address = user.address
            address?.let {
                address.city = etCity.text.toString()
                address.district = etState.text.toString()
                address.street = etStreet.text.toString()
            }
            if (etPostCode.text.isEmpty()) {
                Toast.makeText(this, "Input PostCode!", Toast.LENGTH_LONG).show()
            } else {
                address?.postCode = etPostCode.text.toString().toInt()
            }
            user.address = address
            val updateBook = BookRoomDatabase.getInstance(this).bookDao().updateBook(book = book)
            val updateUser = BookRoomDatabase.getInstance(this).userDao().updateUser(user = user)
            if (updateBook != 0 && updateUser != 0) {
                Toast.makeText(this, "Update success!", Toast.LENGTH_LONG).show()
                finish()
            } else {
                Toast.makeText(this, "Update no success!", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun initData() {
        etTitle.setText(book.title)
        etfirtName.setText(user.firtName)
        etSecondName.setText(user.lastName)
        etStreet.setText(user.address?.street)
        etState.setText(user.address?.district)
        etCity.setText(user.address?.city)
        etPostCode.setText(user.address?.postCode.toString())
        etDate.setText(book.date)
    }
}
