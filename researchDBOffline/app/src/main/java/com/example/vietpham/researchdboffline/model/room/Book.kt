package com.example.vietpham.researchdboffline.model.room

import android.arch.persistence.room.*

@Entity(foreignKeys = [ForeignKey(entity = User::class, parentColumns = ["id"], childColumns = ["user_id"])],
        indices = [(Index("user_id", unique = false))])
class Book {
    @PrimaryKey(autoGenerate = true)
    var bookId: Long = 0
    var title: String = ""
    var date: String = ""

    @ColumnInfo(name = "user_id")
    var userId: Long = 0
}