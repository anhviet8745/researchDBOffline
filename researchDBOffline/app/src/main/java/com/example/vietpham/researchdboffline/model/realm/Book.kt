package com.example.vietpham.researchdboffline.model.realm

import io.realm.RealmObject

open class Book : RealmObject() {
    var id: Long = 0
    var title: String = ""
    var description: String = ""
    var author: String = ""
    var imageURL: String = ""
}