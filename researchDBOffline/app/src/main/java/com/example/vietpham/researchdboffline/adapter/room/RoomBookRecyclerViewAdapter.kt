package com.example.vietpham.researchdboffline.adapter.room

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.example.vietpham.researchdboffline.BookRoomDatabase
import com.example.vietpham.researchdboffline.R
import com.example.vietpham.researchdboffline.activity.room.RoomDetailBookActivity
import com.example.vietpham.researchdboffline.model.room.Book
import kotlinx.android.synthetic.main.item_book_room.view.*

class RoomBookRecyclerViewAdapter(val data: List<Book>, val context: Context) : RecyclerView.Adapter<RoomBookRecyclerViewAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_book_room, p0, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
        val book = data[p1]
        p0.tvTitle.text = book.title
        p0.tvDate.text = book.date
        p0.vgBook.setOnClickListener {
            val intent = Intent(context, RoomDetailBookActivity::class.java)
            intent.putExtra("bookId", book.bookId)
            context.startActivity(intent)
        }

        p0.vgBook.setOnLongClickListener {
            val book = BookRoomDatabase.getInstance(context).bookDao().getBookById(data[p1].bookId)
            val deleteBook = BookRoomDatabase.getInstance(context).bookDao().deleteBook(book)
            if (deleteBook != 0) {
                Toast.makeText(context, "Delete success", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(context, "Delete no success", Toast.LENGTH_LONG).show()
            }
            return@setOnLongClickListener true
        }

    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvTitle: TextView = view.tvTitle
        val tvDate: TextView = view.tvDate
        val vgBook: View = view.vgBook
    }
}