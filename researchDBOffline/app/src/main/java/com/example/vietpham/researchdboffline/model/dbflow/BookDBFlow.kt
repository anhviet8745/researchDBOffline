package com.example.vietpham.researchdboffline.model.dbflow

import com.example.vietpham.researchdboffline.DBFlowDatabase
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.structure.BaseModel

@Table(name = "books", database = DBFlowDatabase::class)
class BookDBFlow : BaseModel() {
    @PrimaryKey(autoincrement = true)
    @Column(name = "id")
    var id: Long = 0
    @Column(name = "title")
    var title: String = ""
    @Column(name = "description")
    var description: String = ""
    @Column(name = "author")
    var author: String = ""
    @Column(name = "imageURL")
    var imageURL: String = ""
}