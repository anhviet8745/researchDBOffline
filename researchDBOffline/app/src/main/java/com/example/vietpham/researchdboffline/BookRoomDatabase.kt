package com.example.vietpham.researchdboffline

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.example.vietpham.researchdboffline.model.room.Book
import com.example.vietpham.researchdboffline.model.room.BookDAO
import com.example.vietpham.researchdboffline.model.room.User
import com.example.vietpham.researchdboffline.model.room.UserDAO

@Database(entities = [Book::class,User::class], version = 1)
abstract class BookRoomDatabase : RoomDatabase() {
    abstract fun bookDao(): BookDAO
    abstract fun userDao() : UserDAO

    companion object {
        private var sInstance: BookRoomDatabase? = null

        @Synchronized
        fun getInstance(context: Context): BookRoomDatabase {
            if (sInstance == null) {
                sInstance = Room.databaseBuilder(context.applicationContext, BookRoomDatabase::class.java, "BookRoom")
                        .allowMainThreadQueries()
                        .build()
            }
            return sInstance!!
        }
    }
}