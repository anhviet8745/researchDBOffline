package com.example.vietpham.researchdboffline.model.room

import android.arch.persistence.room.ColumnInfo

class Address {
    var street: String = ""
    var district: String = ""
    var city: String = ""

    @ColumnInfo(name = "post_code")
    var postCode: Int = 0
}