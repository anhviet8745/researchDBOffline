package com.example.vietpham.researchdboffline.activity.dbflow

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import com.example.vietpham.researchdboffline.R
import com.example.vietpham.researchdboffline.adapter.dbflow.DBFlowBookRecyclerViewAdapter
import com.example.vietpham.researchdboffline.model.dbflow.BookDBFlow
import com.raizlabs.android.dbflow.sql.language.SQLite
import kotlinx.android.synthetic.main.activity_dbflow_update.*


class DBFlowUpdateActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dbflow_update)
        val bookDBFlow = SQLite.select().from(BookDBFlow::class.java).queryList()
        val adapter = DBFlowBookRecyclerViewAdapter(bookDBFlow, this)
        rvDate.layoutManager = LinearLayoutManager(this)
        rvDate.itemAnimator = DefaultItemAnimator()
        rvDate.adapter = adapter
        adapter.notifyDataSetChanged()

        btUpdate.setOnClickListener {
            bookDBFlow[0].title = "Toi la DBFlow"
            bookDBFlow[0].description = "Toi cung la DBFlow"
            bookDBFlow[0].author = "Toi la nguoi sang tac DBFlow"
            bookDBFlow[0].imageURL = "19/08/2608"
            bookDBFlow[0].update()
        }
    }
}
