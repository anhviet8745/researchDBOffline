package com.example.vietpham.researchdboffline.activity.room

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.vietpham.researchdboffline.BookRoomDatabase
import com.example.vietpham.researchdboffline.R
import com.example.vietpham.researchdboffline.model.room.Address
import com.example.vietpham.researchdboffline.model.room.Book
import com.example.vietpham.researchdboffline.model.room.User
import kotlinx.android.synthetic.main.activity_room_insert.*
import org.jetbrains.anko.doAsync

class RoomInsertActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_room_insert)
        btInsert.setOnClickListener {
            val book = Book()
            book.title = etTitle.text.toString()
            book.date = etDate.text.toString()
            val user = User()
            user.firtName = etfirtName.text.toString()
            user.lastName = etSecondName.text.toString()
            val address = Address()
            address.city = etCity.text.toString()
            address.district = etState.text.toString()
            address.street = etStreet.text.toString()
            address.postCode = etPostCode.text.toString().toInt()
            user.address = address
            val idUser = BookRoomDatabase.getInstance(this@RoomInsertActivity).userDao().insertUser(user)
            if (idUser.toInt() != 0) {
                Log.e("idUser", "$idUser")
                book.userId = idUser
                val idBook = BookRoomDatabase.getInstance(this@RoomInsertActivity).bookDao().insertBook(book)
                if (idBook.toInt() != 0) {
                    Toast.makeText(this@RoomInsertActivity, "Insert book success!", Toast.LENGTH_LONG).show()
                    finish()
                } else {
                    Toast.makeText(this@RoomInsertActivity, "Insert book no success!", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(this@RoomInsertActivity, "Insert User no success!", Toast.LENGTH_LONG).show()
            }
        }
    }
}
