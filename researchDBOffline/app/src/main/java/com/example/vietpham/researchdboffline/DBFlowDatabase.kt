package com.example.vietpham.researchdboffline

import com.raizlabs.android.dbflow.annotation.Database

@Database(name = DBFlowDatabase.NAME, version = DBFlowDatabase.VERSION, generatedClassSeparator = "_")
object DBFlowDatabase {
    const val NAME: String = "app"
    const val VERSION: Int = 1
}