package com.example.vietpham.researchdboffline.model.room

import android.arch.persistence.room.*

@Dao
interface UserDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertUser(user: User): Long

    @Query("SELECT * FROM user WHERE id = :id")
    fun getUserById(id: Long): User

    @Query("SELECT * FROM user")
    fun getUsers(): List<User>

    @Update
    fun updateUser(user: User) : Int

    @Delete
    fun deleteUser(user: User) : Int
}