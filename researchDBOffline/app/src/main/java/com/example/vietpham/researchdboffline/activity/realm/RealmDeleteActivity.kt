package com.example.vietpham.researchdboffline.activity.realm

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.example.vietpham.researchdboffline.R
import com.example.vietpham.researchdboffline.model.realm.Book
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.activity_realm_delete.*

class RealmDeleteActivity : AppCompatActivity() {
    private val realm = Realm.getDefaultInstance()
    var id: Long = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_realm_delete)

        btDelete.setOnClickListener {
            if (etID.text.isEmpty()) {
                Toast.makeText(this, "Hay nhap Id can xoa", Toast.LENGTH_LONG).show()
            } else {
                id = etID.text.toString().toLong()
                realm.executeTransaction {
                    val resultRealm = realm.where<Book>().equalTo("id", id).findAll()
                    if (resultRealm.deleteAllFromRealm()) {
                        Toast.makeText(this, "Xoa Thanh Cong", Toast.LENGTH_LONG).show()
                        finish()
                    } else {
                        Toast.makeText(this, "Xoa khong thanh cong", Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }
}
