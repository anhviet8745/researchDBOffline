package com.example.vietpham.researchdboffline.adapter.realm

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.vietpham.researchdboffline.R
import com.example.vietpham.researchdboffline.model.realm.Book
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter
import kotlinx.android.synthetic.main.item_realm.view.*


class RealmBookRecyclerViewAdapter(data: OrderedRealmCollection<Book>) : RealmRecyclerViewAdapter<Book, RealmBookRecyclerViewAdapter.MyViewHolder>(data, true) {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        val itemView = LayoutInflater.from(p0.context).inflate(R.layout.item_realm, p0, false)
        return MyViewHolder(itemView)
    }

    override fun getItemId(position: Int): Long {
        return getItem(position)!!.id
    }

    override fun getItemCount(): Int {
        return data!!.size
    }


    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
        val item = data?.get(p1)
        p0.tvTitle.text = item?.title
        p0.tvDescription.text = item?.description
        p0.tvAuthor.text = item?.author
        p0.tvDate.text = item?.imageURL
        p0.tvId.text = item?.id.toString()
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvId : TextView = view.tvId
        val tvTitle: TextView = view.tvTitle
        val tvDescription: TextView = view.tvDescription
        val tvAuthor: TextView = view.tvAuthor
        val tvDate: TextView = view.tvDate
    }
}

