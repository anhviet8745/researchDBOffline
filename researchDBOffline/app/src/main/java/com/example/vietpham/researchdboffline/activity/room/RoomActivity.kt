package com.example.vietpham.researchdboffline.activity.room

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import com.example.vietpham.researchdboffline.BookRoomDatabase
import com.example.vietpham.researchdboffline.R
import com.example.vietpham.researchdboffline.adapter.room.RoomBookRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_main4.*
import kotlinx.android.synthetic.main.content_main.*

class RoomActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main4)

        initData()
        addEvent()
    }

    private fun addEvent() {
        fab.setOnClickListener {
            val intent = Intent(this, RoomInsertActivity::class.java)
            startActivity(intent)
        }
    }

    private fun initData() {
        val books = BookRoomDatabase.getInstance(this).bookDao().getBooks()
        val roomAdapter = RoomBookRecyclerViewAdapter(books, this)
        rvBook.layoutManager = LinearLayoutManager(this)
        rvBook.itemAnimator = DefaultItemAnimator()
        rvBook.adapter = roomAdapter
        roomAdapter.notifyDataSetChanged()
    }
}
