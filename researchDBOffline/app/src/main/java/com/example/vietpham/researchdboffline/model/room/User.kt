package com.example.vietpham.researchdboffline.model.room

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.graphics.Bitmap

@Entity
class User {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
    var firtName: String = ""
    var lastName: String = ""

    @Embedded
    var address: Address? = null

    @Ignore
    var picture: Bitmap? = null
}