package com.example.vietpham.researchdboffline.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.example.vietpham.researchdboffline.R
import com.example.vietpham.researchdboffline.model.realm.Book
import com.example.vietpham.researchdboffline.model.dbflow.BookDBFlow
import com.example.vietpham.researchdboffline.BookRoomDatabase
import com.example.vietpham.researchdboffline.activity.dbflow.DBFlowActivity
import com.example.vietpham.researchdboffline.activity.realm.RealmActivity
import com.example.vietpham.researchdboffline.activity.room.RoomActivity
import io.realm.Realm
import io.realm.kotlin.createObject
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.doAsync
import java.util.*

class MainActivity : AppCompatActivity() {
    private val realm = Realm.getDefaultInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.e("Test", "onCreate")
        setContentView(R.layout.activity_main)

        realm.executeTransaction { realm ->
            realm.deleteAll()
        }

        val book1 = Book()
        book1.title = "Avp"
        book1.description = "Anh Viet Pham 1"
        book1.author = "Viet Pham Anh Viet 1"
        book1.imageURL = "20/08/2018"

        val book2 = Book()
        book2.title = "Avp 2"
        book2.description = "Anh Viet Pham 2"
        book2.author = "test"
        book2.imageURL = "17/05/1995"

        val book3 = Book()
        book3.title = "Avp 3"
        book3.description = "Anh Viet Pham 3"
        book3.author = "Viet Pham Anh Viet 3"
        book3.imageURL = "24/08/2018"

        val book4 = Book()
        book4.title = "Avp 4"
        book4.description = "Anh Viet Pham 4"
        book4.author = "Viet Pham Anh Viet 4"
        book4.imageURL = "15/10/158"

        val books = Arrays.asList(book1, book2, book3, book4)

        realm.executeTransaction { realm ->
            books.forEachIndexed { index, book ->
                val bookRealm = realm.createObject<Book>()
                bookRealm.title = book.title
                bookRealm.description = book.description
                bookRealm.author = book.author
                bookRealm.imageURL = book.imageURL
            }
        }
        realm.close()

        val bookDBFlow = BookDBFlow()
        bookDBFlow.title = "VPA DBFLOW"
        bookDBFlow.description = "I am Viet Pham Anh"
        bookDBFlow.author = "Viet Pham Anh"
        bookDBFlow.imageURL = "5151a6d1sadasdas"
        bookDBFlow.save()

        val bookDBFlow1 = BookDBFlow()
        bookDBFlow1.title = "VPA DBFLOW2"
        bookDBFlow1.description = "I am Viet Pham Anh"
        bookDBFlow1.author = "Viet Pham Anh"
        bookDBFlow1.imageURL = "5151a6d1sadasdas"
        bookDBFlow1.save()

        val bookDBFlow2 = BookDBFlow()
        bookDBFlow2.title = "VPA DBFLOW3"
        bookDBFlow2.description = "I am Viet Pham Anh"
        bookDBFlow2.author = "Viet Pham Anh"
        bookDBFlow2.imageURL = "5151a6d1sadasdas"
        bookDBFlow2.save()

        val bookDBFlow3 = BookDBFlow()
        bookDBFlow3.title = "VPA DBFLOW5"
        bookDBFlow3.description = "I am Viet Pham Anh"
        bookDBFlow3.author = "Viet Pham Anh"
        bookDBFlow3.imageURL = "5151a6d1sadasdas"
        bookDBFlow3.save()

        val bookDBFlow4 = BookDBFlow()
        bookDBFlow4.title = "VPA DBFLOW10"
        bookDBFlow4.description = "I am Viet Pham Anh"
        bookDBFlow4.author = "Viet Pham Anh"
        bookDBFlow4.imageURL = "5151a6d1sadasdas"
        bookDBFlow4.save()

        val bookDBFlow5 = BookDBFlow()
        bookDBFlow5.title = "VPA DBFLOW12"
        bookDBFlow5.description = "I am Viet Pham Anh"
        bookDBFlow5.author = "Viet Pham Anh"
        bookDBFlow5.imageURL = "5151a6d1sadasdas"
        bookDBFlow5.save()






        btClickRealm.setOnClickListener {
            val intent = Intent(this, RealmActivity::class.java)
            startActivity(intent)
        }

        btClickDBFlow.setOnClickListener {
            val intent = Intent(this, DBFlowActivity::class.java)
            startActivity(intent)
        }

        btClickDBRoom.setOnClickListener {
            val intent = Intent(this, RoomActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onStart() {
        super.onStart()
        Log.e("Test", "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.e("Test", "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.e("Test", "onPause")
    }

    override fun onRestart() {
        super.onRestart()
        Log.e("Test", "onRestart")
    }

    override fun onStop() {
        super.onStop()
        Log.e("Test", "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("Test", "onDestroy")
    }

}
