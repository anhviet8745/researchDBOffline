package com.example.vietpham.researchdboffline.activity.dbflow

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import com.example.vietpham.researchdboffline.R
import com.example.vietpham.researchdboffline.adapter.dbflow.DBFlowBookRecyclerViewAdapter
import com.example.vietpham.researchdboffline.model.dbflow.BookDBFlow
import com.raizlabs.android.dbflow.sql.language.SQLite
import kotlinx.android.synthetic.main.activity_main3.*

class DBFlowInsertActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dbflow_insert)
        val bookDBFlow = SQLite.select().from(BookDBFlow::class.java).queryList()
        val adapter = DBFlowBookRecyclerViewAdapter(bookDBFlow, this)
        rvDate.layoutManager = LinearLayoutManager(this)
        rvDate.itemAnimator = DefaultItemAnimator()
        rvDate.adapter = adapter
    }
}
