package com.example.vietpham.researchdboffline.activity.realm

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.example.vietpham.researchdboffline.R
import com.example.vietpham.researchdboffline.adapter.realm.RealmBookRecyclerViewAdapter
import com.example.vietpham.researchdboffline.model.realm.Book
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.activity_realm_update.*


class RealmUpdateActivity : AppCompatActivity() {
    private val realm = Realm.getDefaultInstance()
    private var authorText: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_realm_update)
        val dogs = realm.where<Book>().findAll()
        val adapter = RealmBookRecyclerViewAdapter(realm.where<Book>().findAll())
        rvData.layoutManager = LinearLayoutManager(this)
        rvData.itemAnimator = DefaultItemAnimator()
        rvData.adapter = adapter

        btUpdate.setOnClickListener {
            if (etInputAuthor.text.isEmpty()) {
                Toast.makeText(this, "Hay nhap author can update", Toast.LENGTH_LONG).show()
            } else {
                authorText = etInputAuthor.text.toString()
                val resultRealm = realm.where<Book>().equalTo("author", authorText).findFirst()
                realm.executeTransaction {
                    resultRealm?.author = "King of men"
                }
            }
        }



    }
}


