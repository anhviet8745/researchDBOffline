package com.example.vietpham.researchdboffline.activity.dbflow

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.example.vietpham.researchdboffline.R
import com.example.vietpham.researchdboffline.adapter.dbflow.DBFlowBookRecyclerViewAdapter
import com.example.vietpham.researchdboffline.model.dbflow.BookDBFlow
import com.raizlabs.android.dbflow.sql.language.SQLite
import kotlinx.android.synthetic.main.activity_dbflow_delete.*

class DBFlowDeleteActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dbflow_delete)
        val bookDBFlow = SQLite.select().from(BookDBFlow::class.java).queryList()
        val adapter = DBFlowBookRecyclerViewAdapter(bookDBFlow, this)
        rvDate.layoutManager = LinearLayoutManager(this)
        rvDate.itemAnimator = DefaultItemAnimator()
        rvDate.adapter = adapter
        adapter.notifyDataSetChanged()
        btDelete.setOnClickListener {
            if (bookDBFlow[0].delete()) {
                Toast.makeText(this, "Delete thanh cong", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this, "Delete khong thanh cong", Toast.LENGTH_LONG).show()
            }
        }
    }
}
