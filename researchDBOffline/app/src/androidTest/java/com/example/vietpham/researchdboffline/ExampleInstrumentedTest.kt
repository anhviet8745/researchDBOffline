package com.example.vietpham.researchdboffline

import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.example.vietpham.researchdboffline.model.room.*
import kotlinx.android.synthetic.main.activity_room_insert.*
import org.junit.After

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Before

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    private var userDao: UserDAO? = null
    private var bookDao: BookDAO? = null
   // private var mDb: BookRoomDatabase? = null

    @Before
    fun createDb() {
//        val context = InstrumentationRegistry.getTargetContext()
//        mDb = Room.databaseBuilder(context, BookRoomDatabase::class.java).build()
//        userDao = mDb?.userDao()
//        bookDao = mDb?.bookDao()
    }

    @After
    fun closeDb() {
//        mDb?.close()
    }

    @Test
    fun useAppContext() {
        // Context of the app under test.
//        val book = Book()
//        book.title = "AVP3"
//        book.date = "20/18/2018"
//        val book1 = Book()
//        book1.title = "AVP3q213"
//        book1.date = "20/18/2018123"
//        val book2 = Book()
//        book2.title = "AVP3123"
//        book2.date = "20/18/2018123"
//
//        val user = User()
//        user.firtName = "Viet Pham4qwe"
//        user.lastName = "Anh Vietqweqw"
//        val user1 = User()
//        user1.firtName = "Viet Pham4342342"
//        user1.lastName = "Anh Vietqweqweqwe12sd"
//        val user2 = User()
//        user2.firtName = "Viet Pham41231sd"
//        user2.lastName = "Anh Viet123sada"
//
//        val address = Address()
//        address.city = "HCM23423"
//        address.district = "Quan 3423423"
//        address.street = "CMT8234234"
//        address.postCode = 123234234
//        user.address = address
//        user1.address = address
//        user2.address = address
//
//        userDao?.insertUser(user)
//        userDao?.insertUser(user1)
//        userDao?.insertUser(user2)
//
//        book.userId = 3
//        book1.userId = 3
//        book2.userId = 3
//
//        bookDao?.insertBook(book)
//        bookDao?.insertBook(book1)
//        bookDao?.insertBook(book2)
        val context = InstrumentationRegistry.getTargetContext()
        val listUser = BookRoomDatabase.getInstance(context = context).userDao().getUsers()
        val listBook = BookRoomDatabase.getInstance(context).bookDao().getBooks()
    }
}
